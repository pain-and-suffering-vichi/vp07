﻿program VP06
!!! Программа выполняет быстрое преобразование Фурье (FFT)

use FFT
implicit none

integer(4) :: N, N1, i
character(7) :: choice
complex, allocatable :: X(:), Y(:), W(:)
real(8), allocatable :: ReImX(:,:), ReImY(:,:)
    
    open(1,file='data.dat')
    read(1,'(2x,I6)') N
    N1=1
    do while (N1<N)
        N1=N1*2
    enddo
    
    allocate(ReImX(2,N1))
    
    read(1,*) ReImX(:,1:N)
    ReImX(:,N+1:N1)=0
    N=N1
    close (1)
    
    allocate(X(N),Y(N),ReImY(2,N),W(0:N-1))
    X=cmplx(ReImX(1,:),ReImX(2,:))
    deallocate(ReImX)

    write(*,*) "Enter 'direct' for the direct Fourier transform, 'inverse' for the inverse one."
    
    call getarg(7,choice)
    choice = 'direct'
    select case(choice)
        case('direct')
            forall (i=0:N-1) W(i) = expMinus(i,N)
            call BitReversalSorting(X,Y,W)
        case('inverse')
            forall (i=0:N-1) W(i) = expPlus(i,N)
            call BitReversalSorting(X,Y,W)
        case default; stop 'Please, enter type of the Fourier transform'
        end select
        
    Y=Y/sqrt(real(N))
    deallocate(X)
    
    open(2,file='abs.dat')
    write(2,*) '# ', N
    do i=1,N
        write(2,*) abs(Y(i))
    enddo
    close(2)
    
    ReImY(1,:)=real(Y)
    ReImY(2,:)=aimag(Y)
    
    open(3,file='result.dat')
    write(3,*) '# ', N
    do i=1,N
        write(3,*) ReImY(:,i)
    enddo
    close(3)
    
    deallocate(Y,ReImY)
end program VP06
    